#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
from itertools import product

# constants and translations
opac=0.9
fontsize=16
ticklabelsize=8

lstf = ["lotz", "omm", "cocz"]
lsttoplot = ["time", "gen"]
df, lstn = None, None # slot holders

func2name = {
    "lotz"  : r"\textsc{LOTZ}", 
    "omm"   : r"\textsc{OMM}", 
    "cocz"  : r"\textsc{COCZ}", 
}
data2name = {
    "time"  : r"\textsc{CPU} time", 
    "gen"   : r"\# generations", 
}
sel2name  = {
    "bin"  : r"\textsc{Bin}", 
    "pow"  : r"\textsc{Pow}$_{\mathrm{fixed}}$", 
    "tpow" : r"\textsc{Pow}$_{\mathrm{adjusted}}$", 
    "sto"  : r"\textsc{Sto}", 
    "fsto" : r"\textsc{Sto}$_{\mathrm{fixed}}$", 
    "tsto" : r"\textsc{Sto}$_{\mathrm{adjusted}}$", 
}
sel2color = {
    "bin"  : "black", 
    "pow"  : "purple", 
    "tpow" : "blue", 
    "sto"  : "red", 
    "fsto" : "green", 
    "tsto" : "orange", 
}

lstsmalln = list(range(20,120+1,10))
lstsmallsel = ["bin", "pow", "tpow", "sto", "fsto", "tsto"]

lstbign = list(range(20,400+1,20))
lstbigsel = ["pow", "tpow", "sto", "fsto", "tsto"]
lstbigselzoom = ["tpow", "sto", "tsto"]


# read and create the dataframe
df = None
def load_data(filename): 
    global df
    df = pd.read_csv(filename, sep=";", header=0,
                     names=["func", "n", "sel", "mu", "seed", "gen", "time"])
    return df

# extract and plot one data series
def extract_data(func, n, sel, data):
    return df.loc[(df["func"]==func) & (df["n"]==n) & (df["sel"]==sel)][data].tolist()

def plot_data(ax, func, sel, data, col):
    return ax.boxplot([extract_data(func, n, sel, data)
                       for n in lstn], labels=lstn,
                      patch_artist=True, 
                      boxprops=dict(facecolor=col, color=col, alpha=opac),
                      capprops=dict(color=col, alpha=opac),
                      whiskerprops=dict(color=col, alpha=opac),
                      flierprops=dict(color=col, markeredgecolor=col, alpha=opac))
    
def plot_all(ax, func, data, lstsel, rotate=0):
    lstbp = [plot_data(ax, func, sel, data, col=sel2color[sel]) 
             for sel in lstsel]
    ax.legend([bp["boxes"][0] for bp in lstbp],
              [sel2name[sel] for sel in lstsel], 
              loc="upper left")
    ax.set_title(func2name[func])
    ax.set_xlabel(r"$n$")
    ax.set_ylabel(data2name[data])
    ax.tick_params(axis='x', labelrotation=rotate, labelsize=ticklabelsize)
    ax.tick_params(axis='both', labelsize=ticklabelsize)

# extract and plot the ratio between two data series
def extract_ratio_data(func, n, sel, data1, data2):
    d1 = df.loc[(df["func"]==func) & (df["n"]==n) & (df["sel"]==sel)][data1]
    d2 = df.loc[(df["func"]==func) & (df["n"]==n) & (df["sel"]==sel)][data2]
    return (d1 / d2).tolist()

def plot_ratio_data(ax, func, sel, data1, data2, col):
    return ax.boxplot([extract_ratio_data(func, n, sel, data1, data2)
                       for n in lstn], labels=lstn,
                      patch_artist=True, 
                      boxprops=dict(facecolor=col, color=col, alpha=opac),
                      capprops=dict(color=col, alpha=opac),
                      whiskerprops=dict(color=col, alpha=opac),
                      flierprops=dict(color=col, markeredgecolor=col, alpha=opac))

def plot_ratio_all(ax, func, data1, data2, lstsel, rotate=0):
    lstbp = [plot_ratio_data(ax, func, sel, data1, data2, col=sel2color[sel]) 
             for sel in lstsel]
    ax.legend([bp["boxes"][0] for bp in lstbp],
              [sel2name[sel] for sel in lstsel], 
              loc="upper left")
    ax.set_title(func2name[func])
    ax.set_xlabel(r"$n$")
    ax.set_ylabel(r"{} / ({})".format(data2name[data1],data2name[data2]))
    ax.tick_params(axis='x', labelrotation=rotate, labelsize=ticklabelsize)
    ax.tick_params(axis='both', labelsize=ticklabelsize)
    
    
if __name__ == "__main__":
    
    plt.rcParams['text.usetex'] = True    
    plt.rcParams['font.size'] = fontsize   
    
    # -- small dimensions
    load_data("results/output-n20to120.txt")
    lstn = lstsmalln
    
    # plot all mechanisms
    fig, ax = plt.subplots(len(lstf), len(lsttoplot)+1, figsize=(24,24))
    for i in range(len(lsttoplot)):
        for j in range(len(lstf)): 
            plot_all(ax[j,i], lstf[j], lsttoplot[i], lstsmallsel)
    for j in range(len(lstf)): 
        plot_ratio_all(ax[j,len(lsttoplot)], lstf[j], lsttoplot[0], lsttoplot[1], lstsmallsel)

    plt.savefig("figures/n20to120.pdf")
    plt.close()    
    
    
    # -- high dimensions
    load_data("results/output-n20to400.txt")
    lstn = lstbign
    
    # plot all mechanisms
    fig, ax = plt.subplots(len(lstf), len(lsttoplot)+1, figsize=(24,24))
    for i in range(len(lsttoplot)):
        for j in range(len(lstf)): 
            plot_all(ax[j,i], lstf[j], lsttoplot[i], lstbigsel)
    for j in range(len(lstf)): 
        plot_ratio_all(ax[j,len(lsttoplot)], lstf[j], lsttoplot[0], lsttoplot[1], lstbigsel)

    plt.savefig("figures/n20to400.pdf")
    plt.close()
    
    
    # -- focus on the fastest algorithms
    load_data("results/output-n20to400.txt")
    lstn = lstbign
    
    # plot all mechanisms
    fig, ax = plt.subplots(len(lstf), len(lsttoplot)+1, figsize=(24,24))
    for i in range(len(lsttoplot)):
        for j in range(len(lstf)): 
            plot_all(ax[j,i], lstf[j], lsttoplot[i], lstbigselzoom)
    for j in range(len(lstf)): 
        plot_ratio_all(ax[j,len(lsttoplot)], lstf[j], lsttoplot[0], lsttoplot[1], lstbigselzoom)

    plt.savefig("figures/n20to400zoom.pdf")
    plt.close()
    
