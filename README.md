        IMPLEMENTATION OF STOCHASTIC TOURNAMENT SELECTION  
           AND POWER-LAW RANKING SELECTION FOR NSGA-II  
        
        by Duc Cuong Dang, Andre Opris and Dirk Sudholt

# List of files 
~~~~
nsga2.py, program to produce the experiment, run with --help for more details 
          the implemented selection mechanisms are: 
            bin,  binary tournament 
            pow,  power-law ranking without adjusting equally fit individuals 
            tpow, power-law ranking with adjusting equally fit individuals 
            sto,  standard implementation of stochastic tournament 
            fsto, stochastic tournament selection implement with pre-computed distribution 
                  and without adjustment for equally fit individuals 
            tsto, stochastic tournament selection implement with pre-computed distribution 
                  and with adjustment for equally fit individuals 

plot.py, program to make the plots from the data in the results/ folder 

generate_cmd.py, program to generate a series of commands for experimental purpose 
                 eg. this can be fed to GNU Parallel for running the experiments 

results/*.txt, experimental results 

figures/*.pdf, plots produced by plot.py 
~~~~

# Required Python package
~~~~
deap, numpy: for the main program
pandas, matplotlib: for the plotting program
~~~~ 

# Format of the results
~~~~
column 1: function name (lotz, omm, or cocz) 
column 2: problem dimension 
column 3: selection mechanism used 
column 4: population size used 
column 5: random seed used 
column 6: reported number of generations
column 7: reported cpu time 
~~~~

