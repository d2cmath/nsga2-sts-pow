#!/usr/bin/env python3

from itertools import product
import numpy.random as random

prog = "./nsga2.py"
ngen = "1e8" # above 100^3
seed = [9774,9606,5827,2388,8762,7643,2526,2067,3045,6383,
        510,1530,8263,4098,6768,9029,3311,2507,6793,5413,
        4705,610,5930,5714,729,131,7649,8257,3085,6777,
        8781,7119,3357,3977,1989,2857,3234,8273,5360,5317,
        8572,7320,8369,2663,6918,3791,4574,2483,397,4176,
        8366,642,5406,3913,6759,2098,8328,2544,1953,9943,
        3471,4046,505,1000,387,840,3691,9853,8875,399,
        8118,9086,5866,2689,1502,5129,8830,9064,5419,209,
        1183,8257,3290,5501,6405,192,7207,6604,4292,5894,
        4166,6328,7186,4264,1344,7484,4066,5983,2040,6892] # from random.org

ndim = list(range(20,400+1,20))
func = ["lotz", "omm", "cocz"]
sel = ["pow", "tpow", "sto", "fsto", "tsto"]

def compute_mu(f, n, factor=2): 
    """ compute the proper population size for LOTZ, COCZ, OMM """    
    
    return factor*(n//2+1 if f=="cocz" else n+1)

def generate(func, ndim, sel, ntrial, shuffle=False):
    """ combine options and generate all combination """
    
    out = ["./nsga2.py -g {} -f {} -n {} -s {} -m {} -r {}".format(ngen, f, n, s, compute_mu(f, n, 2), r) # factor=2 since we use a stable sort
           for f, n, s, r in product(func, ndim, sel, seed[:ntrial])]
    
    if shuffle: random.shuffle(out)
    
    return out

if __name__ == "__main__":
    
    for cmd in generate(func, ndim, sel, ntrial=100, shuffle=True):
        print(cmd)
    
