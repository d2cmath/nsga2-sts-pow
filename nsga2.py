#!/usr/bin/env python3

from deap import base
from deap import creator
from deap import tools

from collections import deque
from math import sqrt, ceil, floor, pow, inf

from sys import exit
from timeit import default_timer as timer

import numpy # make sure all randomness use numpy

## parameter of the problem
n, parammu, lenopt = 0, 0, 0
eval_func, is_opt = None, None
dist_sto = None
dist_pow = None
sampler_pow = None
sampler_sto = None

def setup(problem, ndim, mu, exponent=2): # setup the problem and parameters
    global n, lenopt, parammu
    global eval_func, is_opt
    global dist_sto, dist_pow 
    global sampler_pow, sampler_sto
    
    n = ndim # problem dimension
    parammu = mu # population size
    
    # setup the evaluation function, optimality check and Pareto set size
    if problem=="lotz":
        eval_func = eval_LOTZ 
        is_opt = isopt_LOTZ 
        lenopt = n + 1 
    elif problem=="omm":
        eval_func = eval_OMM
        is_opt = isopt_OMM
        lenopt = n + 1 
    elif problem=="cocz":
        eval_func = eval_COCZ
        is_opt = isopt_COCZ
        lenopt = n//2 + 1
    
    dist_pow = compute_dist_pow(parammu, exponent) # distribution for the power-law ranking selection
    sampler_pow = alias_sampler(dist_pow) # sampler for the power-law distribution 
    
    dist_sto = compute_dist_sto(parammu) # distribution for the stochastic tournament selection
    sampler_sto = alias_sampler(dist_sto) # sampler for the stochastic tournament selection
    
## definition of the studied functions
def LO(x):
    for i, v in enumerate(x):
        if v==0: return i
    return len(x)

def TZ(x):
    for i, v in enumerate(reversed(x)):
        if v==1: return i
    return len(x)

def LOTZ(x):    
    yield LO(x)
    yield TZ(x)

def OM(x):
    return sum(x)

def OMM(x):
    om = OM(x)
    yield om
    yield n-om

def CO1(x):
    return sum(x[:n//2])
    
def CO2(x):
    return sum(x[n//2:])

def COCZ(x):
    co1, co2 = CO1(x), CO2(x)
    yield co1 + co2
    yield co1 + n//2 - co2

## evaluation and optimality check
def eval_LOTZ(x):
    return tuple(LOTZ(x))
    
def isopt_LOTZ(f):
    return f[0]+f[1]==n

def eval_OMM(x):
    return tuple(OMM(x))
    
def isopt_OMM(f):
    return True
    
def eval_COCZ(x):
    return tuple(COCZ(x))
    
def isopt_COCZ(f):
    return f[0]+f[1]==3*(n//2)

## compute the power-law distribution
def compute_dist_pow(n, exponent=2):
    
    mass = [1./pow(i+1, exponent) for i in range(n)]
    normf = sum(mass)
    
    return [m/normf for m in mass]

## compute the distribution for stochastic tournament
def compute_dist_sto(n): # fast version in O(n) based on the formulation in the proof
           
    vi = [((1. - pow(1. - i/n, n+1))/i if i>0 else 1. + 1./n)
          for i in range(n+1)]

    return [u-l for u,l in zip(vi[:-1],vi[1:])]

def compute_dist_sto_slow(n): # slow version in O(n^2) for sanity check only
    
    def ccpi(n, i, k):
        return pow(1 - i/n, k)
        
    return [sum(ccpi(n, i, k+1) - ccpi(n, i+1, k+1) for k in range(n))/n 
            for i in range(n)]

## alias sampler for a custom distribution
class alias_sampler:
    
    def __init__(self, prob):
        """ constructor that builds the alias structure """
        
        # initialisation
        self.n = len(prob)
        self.mass = [p*self.n for p in prob]
        self.alias = [-1]*self.n
        
        # redistribute the mass evenly
        above = [i for i, p in enumerate(prob) if p*self.n>1.]
        below = [i for i, p in enumerate(prob) if p*self.n<1.]        
        while above and below:
            a, b = above[-1], below.pop()
            
            self.alias[b] = a
            self.mass[a] = self.mass[a] + self.mass[b] - 1.
            
            if self.mass[a]<1.:
                above.pop()
                below.append(a)
            elif self.mass[a]==1.:
                above.pop()
        
    def sample(self, nsample=1):
        """ sample from the distribution """
        
        u = numpy.random.random(size=nsample).tolist()
        ri = [int(floor(v*self.n)) for v in u]

        ret = [i if (self.alias[i]==-1) 
                    or (v*self.n-i<self.mass[i])
                 else self.alias[i]
               for v, i in zip(u,ri)]
                
        return ret[0] if nsample==1 else ret
         
## the NSGA-II algorithm with different selection mechanisms
from deap.tools.emo import sortLogNondominated

def cx_onepoint(ind1, ind2):
    """ one point crossover that produces two offspring individual """
    
    n = min(len(ind1), len(ind2))
    cxpt = numpy.random.randint(1, n) # different to the library that uses Python's random
    ind1[cxpt:], ind2[cxpt:] = ind2[cxpt:], ind1[cxpt:]

    return ind1, ind2

def mut_bitwise(ind, rate):
    """ fast version of bitwise flip with geometric sampling """    
    
    n, pos = len(ind), numpy.random.geometric(rate)-1
    while pos<n:
        ind[pos] = type(ind[pos])(not ind[pos])        
        pos += numpy.random.geometric(rate)
        
def sel_uniform(pop, k):
    """ select k individuals uniformly at random from the population """    

    return [pop[i] for i in numpy.random.choice(range(len(pop)), size=k)]

def sel_tournament(pop, k, tournsize=2):
    """ select k individuals by tournament selection of fixed size """    
    
    return [min(sel_uniform(pop, tournsize), 
                key=lambda x: (x.fitness.layer,
                               1./x.fitness.crowding_dist if x.fitness.crowding_dist>0 else float(inf))) 
            for i in range(k)]
        
def sel_powerlaw(pop, k):
    """ select k individuals by power-law ranking """
    
    sorted_pop = sorted(pop, key=lambda x: (x.fitness.layer,
                                            1./x.fitness.crowding_dist if x.fitness.crowding_dist>0 else float(inf)))
    
    return [sorted_pop[i] for i in sampler_pow.sample(k)]    

def sel_truepowerlaw(pop, k):
    """ select k individuals by power-law ranking with adjustment for copies """    
    
    sorted_pop = sorted(pop, key=lambda x: (x.fitness.layer,
                                            1./x.fitness.crowding_dist if x.fitness.crowding_dist>0 else float(inf)))
    
    # count copies and sum the corresponding probability masses
    cntprob = [[1, dist_pow[0]]]
    for i, iplus, p in zip(sorted_pop[:-1], sorted_pop[1:], dist_pow[1:]):
        if (i.fitness.layer==iplus.fitness.layer 
            and i.fitness.crowding_dist==iplus.fitness.crowding_dist):
            cntprob[-1][0] += 1
            cntprob[-1][1] += p
        else:
            cntprob.append([1,p])
    
    # rebuild the probability mass and create the sampler
    sampler = alias_sampler(sum([[p/cnt]*cnt for cnt,p in cntprob], []))

    return [sorted_pop[i] for i in sampler.sample(k)]
    
def sel_stochtournament(pop, k):
    """ select k individuals by tournament selection of random size according to Qian and al. """    
    
    tournsize = numpy.random.randint(1, len(pop)+1, size=k).tolist()    
    return [min(sel_uniform(pop, tournsize[i]), 
                key=lambda x: (x.fitness.layer,
                               1./x.fitness.crowding_dist if x.fitness.crowding_dist>0 else float(inf))) 
            for i in range(k)]

def sel_fixedstochtournament(pop, k):
    """ select k individuals by tournament selection of random size using the pre-computed/fixed distribution """    
    
    sorted_pop = sorted(pop, key=lambda x: (x.fitness.layer,
                                            1./x.fitness.crowding_dist if x.fitness.crowding_dist>0 else float(inf)))
    
    return [sorted_pop[i] for i in sampler_sto.sample(k)]    

def sel_truestochtournament(pop, k):
    """ select k individuals by tournament selection of random size using the pre-computed distribution with adjustment for copies """    
    
    sorted_pop = sorted(pop, key=lambda x: (x.fitness.layer,
                                            1./x.fitness.crowding_dist if x.fitness.crowding_dist>0 else float(inf)))
    
    # count copies and sum the corresponding probability masses
    cntprob = [[1, dist_sto[0]]]
    for i, iplus, p in zip(sorted_pop[:-1], sorted_pop[1:], dist_sto[1:]):
        if (i.fitness.layer==iplus.fitness.layer 
            and i.fitness.crowding_dist==iplus.fitness.crowding_dist):
            cntprob[-1][0] += 1
            cntprob[-1][1] += p
        else:
            cntprob.append([1,p])
    
    # rebuild the probability mass and create the sampler
    sampler = alias_sampler(sum([[p/cnt]*cnt for cnt,p in cntprob], []))

    return [sorted_pop[i] for i in sampler.sample(k)]

def assign_crowding_dist(layer):
    """ assign crowding distances to individuals in a layer """
    
    if len(layer) == 0: return

    dist = [0.0]*len(layer)
    crowd = [(ind.fitness.values, i) for i, ind in enumerate(layer)] 

    for i in range(len(layer[0].fitness.values)): 
        crowd.sort(key=lambda elem: elem[0][i]) # Python's sort is stable
        dist[crowd[0][1]] = float("inf")
        dist[crowd[-1][1]] = float("inf")
        if crowd[-1][0][i] == crowd[0][0][i]: continue
        
        norm = float(crowd[-1][0][i] - crowd[0][0][i]) # this is different to the library
        for prev, cur, next in zip(crowd[:-2], crowd[1:-1], crowd[2:]):
            dist[cur[1]] += (next[0][i] - prev[0][i]) / norm

    for i, d in enumerate(dist):
        layer[i].fitness.crowding_dist = d 
    
def sel_survival_nsga2(pop, k):
    """ survival selection of nsga-ii """

    layers = sortLogNondominated(pop, k)
    
    for i, l in enumerate(layers):
        assign_crowding_dist(l)
        for ind in l:
            ind.fitness.layer = i
    
    survived = sum(layers[:-1], [])
    avail = k - len(survived)
    if avail > 0:
        last = sorted(layers[-1], 
                      key=lambda x: x.fitness.crowding_dist, 
                      reverse=True)
        survived.extend(last[:avail])
    
    return survived
        
## NSGA-2 algorithm
def run_nsga2(sel, pc=0.9, ngen=10**6, verbose=False):
    """ Run NSGA-2 until all Pareto-optimal points are covered
        or the number of generations expired
    """
    
    def assign_fitness(pop, fit):
        """ assign fitness to a population """
        
        for ind, f in zip(pop, fit):
            ind.fitness.values = f
        
    def count_optimal(pop):
        """ count the number of pareto-optimal points covered """
        
        opt = set()
        for ind in pop:
            f = ind.fitness.values
            if is_opt(f): opt.add(f)
        return len(opt)
    
    global n, lenopt, parammu
    
    progress, it = [], 0
    
    toolbox = base.Toolbox()
    toolbox.register("array_bool", numpy.random.randint, 0, 2, size=n)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.array_bool)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    
    toolbox.register("evaluate", eval_func)    
    toolbox.register("crossover", cx_onepoint) 
    toolbox.register("mutate", mut_bitwise, rate=1.0/n) 
    toolbox.register("parentselect", sel_func)
    toolbox.register("survivalselect", sel_survival_nsga2)
            
    pop = toolbox.population(n=parammu) 
    assign_fitness(pop, toolbox.map(toolbox.evaluate, pop))
    toolbox.survivalselect(pop, parammu) # force to compute the layers and crowding distances
    progress.append(count_optimal(pop))
    
    if verbose: 
        print("population size={}".format(parammu))
        print("iteration {}: {}/{}".format(it, progress[-1], lenopt))
            
    while it<ngen and progress[-1]<lenopt:
        offspring = list(toolbox.map(toolbox.clone, toolbox.parentselect(pop, len(pop))))
        for p1, p2 in zip(offspring[::2], offspring[1::2]): # p1, p2 are in alternating indices, ie. odd and even
            if numpy.random.random()<pc:
                toolbox.crossover(p1, p2)
            
            toolbox.mutate(p1)
            toolbox.mutate(p2)
        
        assign_fitness(offspring, toolbox.map(toolbox.evaluate, offspring))
        
        pop = toolbox.survivalselect(pop + offspring, parammu) 
        progress.append(count_optimal(pop))
        it += 1
        
        if verbose: print("iteration {}: {}/{}".format(it, progress[-1], lenopt))
    
    return it, progress
    
## utility functions
def exec_timed(func, *arg, **kwarg):
    """ execute a function and return executed time """
    
    start = timer()
    result = func(*arg, **kwarg)
    
    return timer()-start, *result

def exec_and_quit(msg, code, func, *arg, **kwarg):
    """ show the message, execute a function and quit """
    
    print(msg)
    if func: func(*arg, **kwarg)
    exit(code)   

def write_output(data, sep=";", tag=None):
    """ write output data """
    
    if tag!=None: print(tag, end=sep)
    for i, d in enumerate(data):
        print("{}".format(d), end=sep if i+1<len(data) else "\n")
    
if __name__ == "__main__":
    
    from argparse import ArgumentParser 

    parser = ArgumentParser(description="NSGA-II on LOTZ/OMM/COCZ. "
                                        "The output format is: "
                                        "[tag] [func] [ndim] [sel] [mu] [seed] [ngen] [cputime]. "
                                        "This software is free and wild!")

    parser.add_argument("-f", "--func", default="lotz", help="function to run on among {lotz, omm, cocz} (default=lotz)")
    parser.add_argument("-n", "--ndim", type=int, default=20, help="problem dimension (default=20)")
    parser.add_argument("-s", "--sel", default="sto", help="parent selection used among {bin, pow, tpow, sto, fsto, tsto} (default=sto)")
    parser.add_argument("-m", "--mu", type=int, default=42, help="population size (expected to be even, otherwise mu-1 is used) (default=42))")
    parser.add_argument("-p", "--pc", type=float, default=0.9, help="probability of crossover (default=0.9)")
    parser.add_argument("-e", "--expo", type=float, default=2.0, help="exponent used for the power-law ranking (default=2)")
    parser.add_argument("-g", "--maxgen", type=float, default=1e6, help="maximal number of generations allowed (default=1e6)")
    parser.add_argument("-r", "--seed", type=int, help="use a specific random seed (if this is specified)")
    parser.add_argument("-c", "--sep", default=";", help="separator character used in the output (default=;)")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose mode which shows the detail of the progress")
    parser.add_argument("-t", "--tag", help="running tag to add to the output if specified")
    
    args = parser.parse_args()    
    
    # set the random seed
    if args.seed is not None: 
        numpy.random.seed(args.seed)
        
    # checking input
    if args.func not in ["lotz","omm", "cocz"]:
        exec_and_quit("ERR: unknown function to run on, please check the usage below!\n", -1, parser.print_help)
        
    if args.sel=="bin":
        sel_func = sel_tournament
    elif args.sel=="pow":
        sel_func = sel_powerlaw
    elif args.sel=="tpow":
        sel_func = sel_truepowerlaw
    elif args.sel=="sto":
        sel_func = sel_stochtournament
    elif args.sel=="fsto":
        sel_func = sel_fixedstochtournament
    elif args.sel=="tsto":
        sel_func = sel_truestochtournament
    else:
        exec_and_quit("ERR: unknown parent selection mechanism, please check the usage below!\n", -1, parser.print_help)
        
    # setup the problem
    setup(problem=args.func, ndim=args.ndim, mu=args.mu, exponent=args.expo)

    # run nsga-ii
    creator.create("FitnessMax", base.Fitness, weights=(1,)*2)
    creator.create("Individual", list, fitness=creator.FitnessMax)    
    cputime, it, progress = exec_timed(run_nsga2, sel_func, args.pc, ngen=args.maxgen, verbose=args.verbose)

    # output data
    write_output([args.func, args.ndim, 
                  args.sel, args.mu, 
                  args.seed, 
                  len(progress), cputime], args.sep, args.tag)
    
